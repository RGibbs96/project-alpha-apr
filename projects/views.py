from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from projects.forms import CreateProjectForm
from django.contrib.auth.decorators import login_required


@login_required
def project_list(request):
    user = request.user
    project_list = Project.objects.filter(owner=user)

    context = {"project_list": project_list}

    return render(request, "projects/list.html", context)


@login_required
def project_detail_view(request, id):
    project = get_object_or_404(Project, id=id)
    project_tasks = project.tasks.all()

    context = {"project": project, "project_tasks": project_tasks}

    return render(request, "projects/detail.html", context)


@login_required
def project_create(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()

    context = {"form": form}

    return render(request, "projects/create.html", context)
